// name	  : background.js
// author : Simon Descarpentres
// date   : 2017-06, 2020-10
// licence: GPLv3
/* globals browser */
/* Open a new tab, and load "index.html" in it. */
function openMetaPress() {
	console.log('injecting meta-press.es')
	browser.tabs.create({
		'url': '/index.html'
	})
}
browser.browserAction.onClicked.addListener(openMetaPress)

var do_announce_updates = false
if (do_announce_updates)
	/* Announces updates : opens the official link when new version is installed */
	browser.storage.sync.get('version').then(announce_updates, no_version)
async function announce_updates (stored_version) {
	let manifest = await fetch('manifest.json')
	manifest = await manifest.json()
	if (stored_version['version'] != manifest.version) {
		browser.tabs.create({
			'url': 'https://www.meta-press.es/category/journal.html'
		})
		browser.storage.sync.set({version: manifest.version})
	}
}
function no_version() {
	announce_updates(0)
}

/* Schedule Search */
function maj_date(date, frq) {
	var cur_date = new Date()
	let d = date
	if(cur_date > date && frq!='sch_s_stop') {
		if(frq == 'sch_s_hourly') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+3600000))
			}
		}
		if(frq == 'sch_s_daily') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			d = new Date(d.setHours(date.getHours()))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+86400000))
			}
		}
		if(frq == 'sch_s_weekly') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			d = new Date(d.setHours(date.getHours()))
			let mod = (7 - cur_date.getDay() + date.getDay())%7
			d = new Date(d.setTime(d.getTime()+(86400000*mod)))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+(86400000*7)))
			}
		}
		if(frq == 'sch_s_monthly' || frq == 'sch_s_quaterly' || frq == 'sch_s_half-yearly')
		{
			d = new Date(d.setMonth(cur_date.getMonth()))
			d = new Date(d.setFullYear(cur_date.getFullYear()))
			if(frq == 'sch_s_monthly') {
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 1)}
			}
			if(frq == 'sch_s_quaterly') {
				if(!(d.getMonth() - date.getMonth())%3) {
					let mod = (3 - (d.getMonth() - date.getMonth()))%3
					d = d.setMonth(d.getMonth() + mod)
				}
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 3)}
			}
			if(frq == 'sch_s_half-yearly') {
				if(!(d.getMonth() - date.getMonth())%6) {
					let mod = (6 - (d.getMonth() - date.getMonth()))%6
					d = d.setMonth(d.getMonth() + mod)
				}
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 6)}
			}
			if(d.getMonth() < cur_date.getMonth()) {
				d = new Date(d)
				d = d.setFullYear(cur_date.getFullYear()+1)
			}
		}
		if(frq == 'sch_s_annual') {
			d = date.setFullYear(cur_date.getFullYear())
			if(d < cur_date) { d = date.setFullYear(date.getFullYear() + 1)}
		}
	}
	return new Date(d)
}
var table_sch_s
browser.storage.sync.get().then(getElt)
function getElt(elt) {
	var stored_sch_sea = elt.sch_sea
	if(stored_sch_sea) {create_alarm(stored_sch_sea)}
}
async function launch_sch_s(sch_s) {
	var tz = await browser.storage.sync.get('tz').then(elt=>{return elt})
	sch_s = sch_s.name ? sch_s.name : sch_s
	let local_url = new URL(window.location).origin
	let new_url = new URL(local_url +'/index.html' + sch_s)
	let next = new_url.searchParams.get('next')
	let rep = new_url.searchParams.get('rep')
	let last = new Date().toUTCString()
	var tz = await browser.storage.sync.get().then(elt => {return elt['tz']})
	//next = timezoned_date(next, tz) // next in UTC, don't need to set timezone
	next = await maj_date(new Date(next), rep)
	next = next.toUTCString()
	await browser.storage.sync.get().then(elt => {table_sch_s = elt.sch_sea})
	let index = table_sch_s.indexOf(sch_s)
	new_url.searchParams.set('last',last)
	new_url.searchParams.set('next',next)
	table_sch_s[index] = new_url.search
	new_url.searchParams.set('submit','1')
	new_url.searchParams.set('last_r',0)
	browser.storage.sync.set({sch_sea: table_sch_s})
	browser.tabs.create({
		'url': new_url.toString()
	})
	create_alarm(table_sch_s)
}
function create_alarm(sch_s) {
	table_sch_s = sch_s
	browser.alarms.clearAll()
	for(let sch_sea of sch_s) {
		let local_url = new URL(window.location).origin
		var new_url = new URL(local_url +'/index.html' + sch_sea)
		var next = new_url.searchParams.get('next')
		var rep = new_url.searchParams.get('rep')
		var date = new Date(next)
		if(rep != 'sch_s_stop') {
			date = date.getTime()
			browser.alarms.create(sch_sea, {when: date})
			browser.alarms.onAlarm.addListener(launch_sch_s)
		}
	}
	console_debug_alarms(bool_debug)
}
var bool_debug = true
async function console_debug_alarms(bool) {
	if(bool) {
		var alarms
		await browser.alarms.getAll().then(elt => {alarms = elt})
		console.log(alarms)
		for(let i=0; i<alarms.length;i++)
		{
			let date = new Date(alarms[i].scheduledTime)
			console.log("L'alarme ",i," se lancera le ",date)
		}
	}
}
var intlNum = Intl.NumberFormat('fr', {minimumIntegerDigits: 4, useGrouping: 0})
function timezoned_date (dt_str, tz='UTC',nav_to_tz) {
	var dt = dt_str ? new Date(dt_str) : new Date()
	if (isNaN(dt)) throw new Error(`${dt_str} is an invalid Date`)
	if (tz == 'UTC' || tz == 'GMT') return dt
	let parsed_tz = parseInt(tz,10)
	if(isNaN(parsed_tz)) {
		var dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000)
		var dt_UTC = new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000)
		var dt_repr = dt_UTC.toLocaleTimeString('fr', {timeZoneName: 'short', timeZone: tz})
		var int_offset
		if(nav_to_tz) {
			date_tz = dt_UTC
			int_offset = parseInt(-(dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.') * 100))
		}
		else {
			date_tz = dt_orig
			int_offset = parseInt((dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.') * 100))
		}
		var tz_offset = intlNum.format(int_offset)
		var tz_repr = int_offset >= 0 ? '+'+tz_offset : tz_offset
		return new Date(date_tz/*_UTC*/.toISOString().replace(/\.\d{3}Z/, tz_repr))
	}
	else { return new Date(dt) }
}
