// name    : setting_page.js
// author  : Simon Descarpentries, simon /\ acoeuro [] com
// licence : GPLv3
//
/* globals browser CodeMirror List Choices */
import * as µ from './utils.js'
import * as mµ from './mp_utils.js'
import * as g from './gettext_html_auto.js/gettext_html_auto.js'

async function setChoiceTz() {
	var timezone = await fetch('json/zone.tab.json')
	//	https://stackoverflow.com/questions/39263321/javascript-get-html-timezone-dropdown
	timezone = await timezone.json()
	timezone.sort()
	var element = document.getElementById('mp_tz')
	for(let tz of Object.entries(timezone)) {
		var option = document.createElement('option')
		option.text = tz[1]
		option.value = tz[1]
		element.add(option)
	}
	let nav_date = new Date()
	let nav_tz = nav_date.getTimezoneOffset()
	var default_option = document.createElement('option')
	default_option.text = 'Your navigator timezone'
	default_option.value = nav_tz
	element.add(default_option, 0)
}

(async () => {
	'use strict'
	await mµ.set_theme()
	await g.xgettext_html()
	await setChoiceTz()
	var sp_req ={
		autoSearchList: new List('automatic_search', {
			item: 'sch_sea_item',
			valueNames: ['sch_sea_query', 'sch_sea_nb','sch_sea_last',
				{ name: 'sch_sea_url', attr: 'value' },
				{ name: 'sch_sea_id', attr: 'id' },
				{ name: 'input_date', attr: 'value' },
				{ name: 'input_HHMM', attr: 'value' },
				{ name: 'min_date', attr: 'min' },
				{ name: 'title_1', attr: 'title'},
				{ name: 'title_2', attr: 'title'}
			]
		})
	}
	var select_opt = {
		resetScrollPosition: false,
		duplicateItemsAllowed: false,
		searchResultLimit: 8,
		shouldSort : false,
		searchFields: ['label']
	}
	//
	// general settings
	//
	var lang_select = document.getElementById('mp_lang')
	lang_select.value =  await mµ.get_stored_locale()
	lang_select.addEventListener('change', () => {
		browser.storage.sync.set({locale: µ.get_select_value(lang_select)})
		//localStorage.setItem('locale', µ.get_select_value(lang_select))
		location.reload()
	})
	var tz_select = document.getElementById('mp_tz')
	var choice_tz = new Choices(tz_select,Object.assign(select_opt))
	var tz = await mµ.get_stored_tz()
	choice_tz.setChoiceByValue(tz)
	tz_select.addEventListener('change', async() => {
		var tz_value = await µ.get_select_value(tz_select)
		browser.storage.sync.set({tz: tz_value})
		location.reload()
	})
	var bg_select = document.getElementById('dark_background')
	bg_select.value = await mµ.get_stored_theme()
	bg_select.addEventListener('change', () => {
		browser.storage.sync.set({dark_background: µ.get_select_value(bg_select)})
		//localStorage.setItem('dark_background', µ.get_select_value(bg_select))
		location.reload()
	})
	var live_search_reload = document.getElementById('live_search_reload')
	live_search_reload.checked = await mµ.get_stored_live_search_reload()
	live_search_reload.addEventListener('change', () => {
		browser.storage.sync.set({live_search_reload: live_search_reload.checked && '1' || ''})
		//localStorage.setItem('live_search_reload', live_search_reload.checked && '1' || '')
	})
	var sentence_search = document.getElementById('sentence_search')
	sentence_search.checked = await mµ.get_stored_sentence_search()
	sentence_search.addEventListener('change', () => {
		browser.storage.sync.set({sentence_search: sentence_search.checked && '1' || ''})
		//localStorage.setItem('sentence_search', sentence_search.checked && '1' || '')
	})
	var max_res_by_src = document.getElementById('max_res_by_src')
	max_res_by_src.value = await mµ.get_stored_max_res_by_src()
	max_res_by_src.addEventListener('change', () => {
		browser.storage.sync.set({max_res_by_src: max_res_by_src.value})
		//localStorage.setItem('max_res_by_src', max_res_by_src.value)
	})
	var undup_results = document.getElementById('undup_results')
	undup_results.checked = await mµ.get_stored_undup_results()
	undup_results.addEventListener('change', () => {
		browser.storage.sync.set({undup_results: undup_results.checked && '1' || ''})
	//localStorage.setItem('undup_results', undup_results.checked && '1' || '')
	})
	var load_photos = document.getElementById('load_photos')
	load_photos.checked = await mµ.get_stored_load_photos()
	load_photos.addEventListener('change', () => {
		browser.storage.sync.set({load_photos: load_photos.checked && '1' || ''})
	//localStorage.setItem('load_photos', load_photos.checked && '1' || '')
	})
	var headline_loading = document.getElementById('headline_loading')
	headline_loading.checked = await mµ.get_stored_headline_loading()
	headline_loading.addEventListener('change', () => {
		browser.storage.sync.set({headline_loading: headline_loading.checked && '1' || ''})
	//localStorage.setItem('headline_loading', headline_loading.checked && '1' || '')
	})
	var live_headline_reload = document.getElementById('live_headline_reload')
	live_headline_reload.checked = await mµ.get_stored_live_headline_reload()
	live_headline_reload.addEventListener('change', () => {
		browser.storage.sync.set({live_headline_reload: live_headline_reload.checked && '1' || ''})
	//localStorage.setItem('live_headline_reload', live_headline_reload.checked && '1' || '')
	})
	var max_headline_loading = document.getElementById('max_headline_loading')
	max_headline_loading.value = await mµ.get_stored_max_headline_loading()
	max_headline_loading.addEventListener('change', () => {
		browser.storage.sync.set({max_headline_loading: max_headline_loading.value})
	//localStorage.setItem('max_headline_loading', max_headline_loading.value)
	})
	var headline_page_size = document.getElementById('headline_page_size')
	headline_page_size.value = await mµ.get_stored_headline_page_size()
	headline_page_size.addEventListener('change', () => {
		browser.storage.sync.set({headline_page_size: headline_page_size.value})
	//localStorage.setItem('headline_page_size', headline_page_size.value)
	})
	var keep_host_perm = document.getElementById('keep_host_perm')
	keep_host_perm.checked = await mµ.get_stored_keep_host_perm()
	keep_host_perm.addEventListener('change', () => {
		browser.storage.sync.set({keep_host_perm: keep_host_perm.checked && '1' || ''})
	//localStorage.setItem('keep_host_perm', keep_host_perm.checked && '1' || '')
	})
	var provided_sources_json = await fetch('json/sources.json')
	provided_sources_json = await provided_sources_json.json()
	document.getElementById('request_host_perm').addEventListener('click', () => {
		mµ.request_sources_perm(provided_sources_json)
	})
	document.getElementById('drop_host_perm').addEventListener('click', mµ.drop_host_perm)
	const userLang = await mµ.get_wanted_locale()
	// const mp_i18n = await g.gettext_html_auto(userLang)
	//
	// custom_src
	//
	var provided_sources_text = await fetch('json/sources.json')
	provided_sources_text = await provided_sources_text.text()
	var custom_src_codemirror
	document.getElementById('provided_sources').textContent = provided_sources_text
	/* var provided_src_codemirror = */ CodeMirror.fromTextArea(
		document.getElementById('provided_sources'), {
			mode: 'application/json',
			// mode: {name:'javascript', json:true},
			indentWithTabs: true,
			screenReaderLabel: 'CodeMirror',
			readOnly: 'nocursor'
		}
	)
	browser.storage.sync.get('custom_src').then(
		load_custom_src, err => {console.error(`Loading custom_src: ${err}`)}
	)
	function load_custom_src(stored_data){
		if (typeof(stored_data) == 'object' && typeof(stored_data.custom_src) == 'string' &&
				stored_data.custom_src && stored_data.custom_src != '{}') {
			document.getElementById('custom_sources').textContent = stored_data.custom_src
			// document.getElementById('reload_hint').style.display = 'inline'
		} else {
			document.getElementById('custom_sources').textContent =
				document.getElementById('default_custom_sources').textContent
		}
		custom_src_codemirror = CodeMirror.fromTextArea(
			document.getElementById('custom_sources'), {
				mode: 'application/json',
				screenReaderLabel: 'CodeMirror',
				indentWithTabs: true,
				extraKeys: {Tab: false} // avoids keyboard trap preserving 'tab-key' navigation
			}
		)
		const STATUS_CURRENT_LINE = document.getElementById('ln_nb')
		const STATUS_CURRENT_COL = document.getElementById('col_nb')
		custom_src_codemirror.on('cursorActivity', () => {
			const cursor = custom_src_codemirror.getCursor()
			STATUS_CURRENT_LINE.textContent = cursor.line + 1
			STATUS_CURRENT_COL.textContent = cursor.ch + 1
		})
	}
	document.getElementById('save_custom_sources').addEventListener('click', () => {
		custom_src_codemirror.save()
		const custom_src_value = document.getElementById('custom_sources').value
		if (custom_src_value != '')
			try {
				JSON.parse(custom_src_value)
			} catch (exc) {
				alert(`Error parsing user custom source (${exc}).`)
				return
			}
		browser.storage.sync.set({custom_src: custom_src_value})
		document.getElementById('reload_hint').style.display = 'inline'
		document.getElementById('reload_hint').style['font-weight'] = 'bold'
	})
	document.getElementById('reset_custom_sources').addEventListener('click', () => {
		browser.storage.sync.set({custom_src: ''})
	})
	//
	// auto search
	//
	var last, next, rep, date, dt, nav_to_tz
	function b_timezoned_date(elt) {
		return elt.timezoned_date(dt,tz,nav_to_tz)
	}
	function b_maj_date(elt) {
		set_timezone(next)
		return elt.maj_date(next,rep)
	}
	async function set_timezone(date,bool=true) {
		var getting = browser.runtime.getBackgroundPage()
		tz = await mµ.get_stored_tz()
		nav_to_tz = bool
		dt = date
		if (date && date!=0) {date = await getting.then(b_timezoned_date)}
		return date
	}
	function schedule_search(elt) {
		var table_sch_s = elt.sch_sea ? elt.sch_sea : elt
		suite_schedule_search(table_sch_s)
	}
	async function suite_schedule_search(table_sch_s) {
		var getting = browser.runtime.getBackgroundPage()
		var table_sch_s_length = table_sch_s.length
		if (table_sch_s_length) {
			document.getElementById('automatic_search').style.display = 'block'
			document.querySelector('#automatic_search .list').style.visibility = 'visible'
			sp_req.autoSearchList.clear()  // to remove the template
		}
		for(let i=table_sch_s_length;i--;) {
			var intlNum = Intl.NumberFormat('fr', {minimumIntegerDigits: 2, useGrouping: 0})
			var local_url = new URL(window.location).origin
			var new_url = new URL(local_url + table_sch_s[i])
			let temp = await mµ.set_text_params(new_url)
			var list_p = temp['list_p']
			var params = temp['params']
			last = new_url.searchParams.get('last')
			last = last==0 ? 0 : new Date(last)
			if(last!=0) {
				last = await set_timezone(last)
				var last_hm = intlNum.format(last.getHours()) +':'+intlNum.format(last.getMinutes())
				var last_ymd = last.getFullYear() +'-'+ intlNum.format(
					last.getMonth()+1) +'-'+ intlNum.format(last.getDate())
			}
			rep = new_url.searchParams.get('rep')
			next = new_url.searchParams.get('next')
			next = await getting.then(b_maj_date)
			date = new Date(next).toUTCString()
			next = date
			let id = `sch_sea__${i}`
			new_url = new URL(local_url + '/index.html' + new_url.search)
			new_url.searchParams.set('next',next)
			new_url.searchParams.set('id_sch_s',id)
			table_sch_s[i] = new_url.search
			date = new Date()
			date = await set_timezone(date)
			date = date.toISOString()
			var next_tz = await set_timezone(next)
			var next_hm = intlNum.format(next_tz.getHours()) +':'+intlNum.format(next_tz.getMinutes())
			var next_ymd = next_tz.getFullYear() +'-'+ intlNum.format(
				next_tz.getMonth()+1) +'-'+ intlNum.format(next_tz.getDate())
			sp_req.autoSearchList.add({
				sch_sea_nb: i,
				sch_sea_url: new_url,
				sch_sea_query: list_p['q'],
				title_1 : params,
				title_2 : params,
				sch_sea_last: last==0 ? 0 : last_ymd+' '+last_hm,
				sch_sea_id: id,
				input_date: next_ymd,
				input_HHMM: next_hm,
				min_date: date.slice(0,10)
			})
			document.getElementById(id).parentElement.querySelector('.frq').value = rep
		}
		browser.storage.sync.set({sch_sea: table_sch_s})
		add_all_events(table_sch_s)
		sp_req.autoSearchList.sort('sch_sea_id', { order: "asc" })
		browser.alarms.clearAll()
		tab = table_sch_s
		var getting = browser.runtime.getBackgroundPage()
		await getting.then(b_create_alarm)
	}
	var sch_s
	function b_launch_sch_s(elt) {
		elt.launch_sch_s(sch_s)
	}
	function show_sch_s_save_btn_cb (evt) {
		let local_save_btn = evt.target.parentElement.querySelector('.save_date')
		local_save_btn.style.visibility = 'visible'
	}
	function add_all_events(table_sch_s) {
		for(let i=table_sch_s.length; i--;) {
			let local_url = new URL(window.location).origin
			let new_url = new URL(local_url + table_sch_s[i])
			let id = `sch_sea__${i}`
			let elt = document.getElementById(id).parentElement
			elt.querySelector('.frq').addEventListener('change', () => {
				save_as_date(id, table_sch_s)})
			var save_btn = elt.querySelector('.save_date')
			elt.querySelector('.input_date').addEventListener('change', show_sch_s_save_btn_cb)
			elt.querySelector('.input_HHMM').addEventListener('change', show_sch_s_save_btn_cb)
			save_btn.addEventListener('click', (evt) => {
				save_as_date(id, table_sch_s)
				let local_save_btn = evt.target.parentElement.querySelector('.save_date')
				local_save_btn.style.visibility = 'hidden'
			})
			elt.querySelector('.suppr_sch_sea').addEventListener('click', () => {
				del_sch_sea(elt, table_sch_s)})
			elt.querySelector('.sch_sea_url').addEventListener('click', (evt) => {
				browser.tabs.create({ 'url': evt.target.value })
			})
			elt.querySelector('.play').addEventListener('click', () => {
				sch_s = table_sch_s[i]
				var getting = browser.runtime.getBackgroundPage()
				getting.then(b_launch_sch_s)
			})
			elt.querySelector('.duplicate').addEventListener('click', () => {
				new_url.searchParams.delete('id_sch_s')
				sp_req.autoSearchList.clear()
				add_elt(new_url, table_sch_s)
			})
		}
	}
	function add_sch_sea(elt) {
		var table_sch_s = elt.sch_sea ? elt.sch_sea : []
		var new_sch_sea = elt.new_sch_sea
		add_elt(new_sch_sea, table_sch_s)
	}
	async function add_elt(new_sch_sea, table_sch_s) {
		var getting = browser.runtime.getBackgroundPage()
		date = new Date()
		var next = new Date(date.getFullYear(), date.getMonth(), date.getDate()+1, 0, 0
		).toUTCString()
		if(new_sch_sea) {
			let new_url = new URL(new_sch_sea)
			let id = new_url.searchParams.get('id_sch_s')
			if(!new_url.searchParams.get('next')) {new_url.searchParams.set('next',next)}
			if(!new_url.searchParams.get('last')) {new_url.searchParams.set('last','0')}
			if(!new_url.searchParams.get('rep')) {new_url.searchParams.set('rep','sch_s_stop')}
			new_url = new_url.search
			if(id) {
				id = id.split('__')[1]
				table_sch_s.splice(id,1,new_url)
			}
			else {table_sch_s.push(new_url)}
			browser.storage.sync.set({sch_sea: table_sch_s})
			browser.storage.sync.remove('new_sch_sea')
			schedule_search(table_sch_s)
		}
	}
	async function del_sch_sea(elt, table_sch_s){
		elt = elt.querySelector('td')
		let del_id = elt.id.split('__')[1]
		let del_url = table_sch_s[del_id]
		table_sch_s.splice(del_id, 1)
		sp_req.autoSearchList.clear()
		if(table_sch_s.length == 0) {
			browser.storage.sync.remove('sch_sea')
		} else {
			await browser.storage.sync.set({sch_sea: table_sch_s})
			browser.storage.sync.get().then(getElt)
			browser.alarms.clear(del_url)
		}
	}
	var tab
	function b_create_alarm(elt){
		elt.create_alarm(tab)
	}
	async function save_as_date(id, table_sch_s) {
		var getting = browser.runtime.getBackgroundPage()
		let new_id = id.split('__')[1]
		let local_url = new URL(window.location).origin
		let new_url = new URL(local_url + table_sch_s[new_id])
		let elt = document.getElementById(id).parentElement
		let new_date = elt.querySelector('.input_date').value
		let new_hours = elt.querySelector('.input_HHMM').value
		let new_rep = elt.querySelector('.frq').value
		new_date = new_date.split('-')
		let year = new_date[0]
		let month = new_date[1]
		let day = new_date[2]
		new_hours = new_hours.split(':')
		let hours = new_hours[0]
		let minute = new_hours[1]
		var next = new_url.searchParams.get('next')
		next = new Date(year, month-1, day, hours, minute, 0)
		next = await set_timezone(new Date(next),false)
		next = next.toUTCString()
		if(!elt.querySelector('.input_date:invalid')) {
			new_url.searchParams.set('next',next)
			new_url.searchParams.set('rep',new_rep)
			elt.querySelector('.sch_sea_url').value = local_url + '/index.html'+new_url.search
			table_sch_s[new_id] = new_url.search
			browser.storage.sync.set({sch_sea: table_sch_s})
			browser.alarms.clearAll()
			tab = table_sch_s
			var getting = browser.runtime.getBackgroundPage()
			await getting.then(b_create_alarm)
		}
	}
	await browser.storage.sync.get().then(getElt)
	function getElt(elt) {
		if(elt.new_sch_sea) {add_sch_sea(elt)}
		else if(elt.sch_sea) {schedule_search(elt)}
	}
})()
